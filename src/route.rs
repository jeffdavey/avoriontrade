use crate::record::{Goods, GoodsStop};
use std::fmt::{Display, Formatter};
#[derive(Debug)]
pub struct Route<'a> {
    pub buy: GoodsStop<'a>,
    pub sell: GoodsStop<'a>,
    pub distance: u32,
    pub profit: f32,
    pub value_per_cargo: f32,
    pub potential_money: i32,
    pub cargo_size: f32,
}

impl<'a> Route<'a> {
    pub fn new(buy: GoodsStop<'a>, sell: GoodsStop<'a>) -> Self {
        let value_per_cargo =
            (sell.good.price - buy.good.price) as f32 / sell.good.good_size as f32;
        Self {
            distance: buy.distance(sell.station.coord_x, sell.station.coord_y),
            profit: ((sell.good.price - buy.good.price) as f32 / sell.good.price as f32) * 100.0,
            potential_money: std::cmp::min(
                sell.good.max_stock as i32 - sell.good.stock as i32,
                buy.good.stock as i32,
            ) * (sell.good.price as i32 - buy.good.price as i32),
            cargo_size: sell.good.good_size,
            value_per_cargo,
            buy,
            sell,
        }
    }
}

impl<'a> Display for Route<'a> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{} [{}] -> [{}] distance {} profit {:.2}% value_per_cargo {:.2} size: {:.2} potential: {}",
            self.buy.good.name,
            self.buy.station,
            self.sell.station,
            self.distance,
            self.profit,
            self.value_per_cargo,
            self.cargo_size,
            self.potential_money
        )
    }
}

pub struct RouteBuilder {
    start_x: i32,
    start_y: i32,
    radius: u32,
}

impl Default for RouteBuilder {
    fn default() -> Self {
        Self {
            start_x: 0,
            start_y: 0,
            radius: 10,
        }
    }
}

impl RouteBuilder {
    pub fn new() -> Self {
        RouteBuilder {
            ..Default::default()
        }
    }

    pub fn start_x(&mut self, start_x: i32) -> &mut Self {
        self.start_x = start_x;
        self
    }
    pub fn start_y(&mut self, start_y: i32) -> &mut Self {
        self.start_y = start_y;
        self
    }
    pub fn radius(&mut self, radius: u32) -> &mut Self {
        self.radius = radius;
        self
    }

    pub fn generate(self, goods: &Goods) -> Vec<Route> {
        let mut routes = Vec::new();
        let goods_names = goods.all_goods_buyable();
        for name in goods_names {
            if let Some(buy_points) =
                goods.find_goods_buy(name, self.start_x, self.start_y, Some(self.radius))
            {
                if let Some(sell_points) =
                    goods.find_goods_sell(name, self.start_x, self.start_y, Some(self.radius))
                {
                    for buy_point in buy_points.iter() {
                        let mut most = None;
                        let mut potential = 0;
                        for sell_point in sell_points.iter() {
                            if sell_point.distance(self.start_x, self.start_y) <= self.radius {
                                if buy_point.good.price < sell_point.good.price {
                                    let route = Route::new(buy_point.clone(), sell_point.clone());
                                    if route.potential_money > potential {
                                        potential = route.potential_money;
                                        most = Some(route);
                                    }
                                }
                            }
                        }
                        if let Some(most) = most {
                            routes.push(most);
                        }
                    }
                }
            }
        }
        routes
    }
}
