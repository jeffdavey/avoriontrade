use serde::Deserialize;
use std::path::PathBuf;
use structopt::StructOpt;

mod record;
mod route;

#[derive(StructOpt, Debug)]
enum Command {
    Buy {
        good: String,
    },
    Sell {
        good: String,
    },
    Routes {},
    Shop {
        /// Path to toml file containing shopping list
        #[structopt(parse(from_os_str))]
        list: PathBuf,
    },
}

#[derive(Debug, StructOpt)]
#[structopt(name = "trades", about = "Avorion trade route calculator")]
struct Opt {
    /// Path to directory containing CSV files
    #[structopt(parse(from_os_str))]
    path: PathBuf,

    /// starting coordinates
    #[structopt(short, allow_hyphen_values = true)]
    x: Option<i32>,
    /// Starting y
    #[structopt(short, allow_hyphen_values = true)]
    y: Option<i32>,

    #[structopt(short, long)]
    radius: Option<u32>,

    #[structopt(short, long)]
    count: Option<usize>,

    #[structopt(subcommand)]
    cmd: Command,
}

#[derive(Deserialize)]
struct ShoppingItem {
    name: String,
    amount: usize,
}

#[derive(Deserialize)]
struct ShoppingList {
    items: Vec<ShoppingItem>,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let opt = Opt::from_args();
    if opt.path.is_dir() {
        let goods = record::Goods::load(&opt.path)?;
        let radius = opt.radius;
        let count = opt.count;

        match opt.cmd {
            Command::Buy { good } => {
                let x = opt.x.unwrap_or(0);
                let y = opt.y.unwrap_or(0);

                if let Some(mut found) = goods.find_goods_buy(&good, x, y, radius) {
                    let mut sorted = false;
                    if let Some(start_x) = opt.x {
                        if let Some(start_y) = opt.y {
                            sorted = true;
                            found.sort_by(|a, b| {
                                a.distance(start_x, start_y)
                                    .cmp(&b.distance(start_x, start_y))
                            })
                        }
                    }
                    if !sorted {
                        found.sort_by(|a, b| a.good.price.cmp(&b.good.price));
                    }
                    let count = count.unwrap_or(found.len());
                    found[0..count].iter().for_each(|stop| {
                        println!("{:?}", stop);
                    });
                }
            }
            Command::Sell { good } => {
                let x = opt.x.unwrap_or(0);
                let y = opt.y.unwrap_or(0);

                if let Some(mut found) = goods.find_goods_sell(&good, x, y, radius) {
                    let mut sorted = false;
                    if let Some(start_x) = opt.x {
                        if let Some(start_y) = opt.y {
                            sorted = true;
                            found.sort_by(|a, b| {
                                a.distance(start_x, start_y)
                                    .cmp(&b.distance(start_x, start_y))
                            })
                        }
                    }
                    if !sorted {
                        found.sort_by(|a, b| b.good.price.cmp(&a.good.price));
                    }
                    let count = count.unwrap_or(found.len());
                    found[0..count].iter().for_each(|stop| {
                        println!("{:?}", stop);
                    });
                }
            }
            Command::Routes {} => {
                let start_x = opt.x.unwrap_or(0);
                let start_y = opt.y.unwrap_or(0);

                let mut routes = route::RouteBuilder::new();
                routes.start_x(start_x);
                routes.start_y(start_y);

                if let Some(radius) = radius {
                    routes.radius(radius);
                }

                let mut routes = routes.generate(&goods);
                routes.sort_by(|a, b| b.potential_money.cmp(&a.potential_money));
                let count = if let Some(count) = count {
                    count
                } else {
                    routes.len()
                };

                for i in 0..count {
                    println!("{}", routes[i]);
                }
            }
            Command::Shop { list } => {
                let x = opt.x.unwrap_or(0);
                let y = opt.y.unwrap_or(0);

                let list = std::fs::read_to_string(list)?;
                let shopping: ShoppingList = toml::from_str(&list)?;
                let mut total_spend = 0;
                let mut stops = Vec::new();
                for item in shopping.items {
                    if let Some(mut found) = goods.find_goods_buy(&item.name, x, y, radius) {
                        found.sort_by(|a, b| a.good.price.cmp(&b.good.price));

                        let mut total = item.amount as isize;
                        for good in &found {
                            total -= good.good.stock as isize;

                            if total <= 0 {
                                stops.push((item.name.to_string(), good.clone(), total));
                                break;
                            } else {
                                stops.push((item.name.to_string(), good.clone(), total));
                            }
                        }
                        if total > 0 {
                            println!("{} => No more stock. Need {}", &item.name, total);
                        }
                    } else {
                        println!("{} => Not found", &item.name);
                    }
                }
                stops.sort_by(|a, b| a.1.distance(x, y).cmp(&b.1.distance(x, y)));
                for (item, good, total) in stops {
                    if total <= 0 {
                        println!(
                            "{} => Buy {} for {} at {:?}",
                            &item,
                            (total + good.good.stock as isize),
                            (total + good.good.stock as isize) * good.good.price as isize,
                            good.station
                        );
                        total_spend +=
                            (total + good.good.stock as isize) * good.good.price as isize;
                    } else {
                        println!(
                            "{} => Buy {} for {} at {:?}",
                            &item,
                            good.good.stock,
                            good.good.stock * good.good.price,
                            good.station
                        );
                        total_spend += (good.good.stock * good.good.price) as isize;
                    }
                }
                println!("TOTAL: {}", total_spend);
            }
        }
    } else {
        println!("Specify directory path");
    }
    Ok(())
}
