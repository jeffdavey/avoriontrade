use csv::ReaderBuilder;
use serde::Deserialize;
use std::collections::HashMap;
use std::fmt::{Display, Formatter};
use std::fs::File;
use std::io::BufReader;
use std::path::{Path, PathBuf};
use std::time::SystemTime;

#[derive(Debug, Deserialize)]
pub struct Record {
    action: String,
    name: String,
    price: u32,
    stock: u32,
    max_stock: u32,
    coord_x: i32,
    coord_y: i32,
    station: String,
    good_size: f32,
}

#[derive(Debug)]
pub struct Good {
    pub name: String,
    pub price: u32,
    pub stock: u32,
    pub max_stock: u32,
    pub good_size: f32,
}

impl Good {
    pub fn new(record: &Record) -> Self {
        Self {
            name: record.name.to_string(),
            price: record.price,
            stock: record.stock,
            max_stock: record.max_stock,
            good_size: record.good_size,
        }
    }
}

#[derive(Debug, Eq, PartialEq, Hash)]
pub struct Station {
    pub coord_x: i32,
    pub coord_y: i32,
    pub station: String,
    pub price: u32,
    pub max_stock: u32,
}

impl Station {
    pub fn new(record: &Record) -> Self {
        Self {
            coord_x: record.coord_x,
            coord_y: record.coord_y,
            station: record.station.to_string(),
            price: record.price,
            max_stock: record.max_stock,
        }
    }
}

impl Display for Station {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} - {},{}", self.station, self.coord_x, self.coord_y)
    }
}

pub struct Goods {
    buy: HashMap<String, HashMap<Station, Good>>,
    sell: HashMap<String, HashMap<Station, Good>>,
}

#[derive(Debug)]
pub struct CSVName {
    path: PathBuf,
    date: SystemTime,
}

#[derive(Debug, Clone)]
pub struct GoodsStop<'a> {
    pub station: &'a Station,
    pub good: &'a Good,
}

impl<'a> GoodsStop<'a> {
    pub fn distance(&self, x: i32, y: i32) -> u32 {
        //manhattan distance
        (i32::abs(self.station.coord_x - x) + i32::abs(self.station.coord_y - y)) as u32
    }
}

impl Goods {
    pub fn load(path: &Path) -> Result<Self, std::io::Error> {
        let mut csv_files = path
            .read_dir()?
            .map(|entry| {
                let entry = entry?;
                let metadata = entry.metadata()?;
                let system_time = metadata.created()?;
                Ok(CSVName {
                    path: entry.path(),
                    date: system_time,
                })
            })
            .collect::<Result<Vec<_>, std::io::Error>>()?;

        csv_files.sort_by(|a, b| a.date.cmp(&b.date));

        let mut goods = Goods {
            buy: HashMap::new(),
            sell: HashMap::new(),
        };

        for csv_file in &csv_files {
            let file = File::open(csv_file.path.to_path_buf())?;
            let reader = BufReader::new(file);
            let mut csv_reader = ReaderBuilder::new()
                .delimiter(b';')
                .has_headers(false)
                .from_reader(reader);
            for record in csv_reader.deserialize() {
                let record: Record = record?;
                match record.action.as_str() {
                    "buy" => {
                        let goods = goods
                            .buy
                            .entry(record.name.to_string())
                            .or_insert_with(|| HashMap::new());
                        let station = Station::new(&record);
                        let good = Good::new(&record);
                        goods.insert(station, good);
                    }
                    "sell" => {
                        let goods = goods
                            .sell
                            .entry(record.name.to_string())
                            .or_insert_with(|| HashMap::new());
                        let station = Station::new(&record);
                        let good = Good::new(&record);
                        goods.insert(station, good);
                    }
                    _ => {
                        println!("Unknown action");
                    }
                }
            }
        }

        Ok(goods)
    }

    pub fn all_goods_buyable(&self) -> Vec<&str> {
        self.buy.keys().map(|s| s.as_str()).collect::<Vec<_>>()
    }

    pub fn find_goods_buy(
        &self,
        good: &str,
        x: i32,
        y: i32,
        radius: Option<u32>,
    ) -> Option<Vec<GoodsStop>> {
        if let Some(stations) = self.buy.get(good) {
            Some(
                stations
                    .iter()
                    .filter_map(|(station, good)| {
                        let stop = GoodsStop { station, good };
                        if let Some(radius) = radius {
                            if stop.distance(x, y) <= radius {
                                Some(stop)
                            } else {
                                None
                            }
                        } else {
                            Some(stop)
                        }
                    })
                    .collect::<Vec<_>>(),
            )
        } else {
            None
        }
    }

    pub fn find_goods_sell(
        &self,
        good: &str,
        x: i32,
        y: i32,
        radius: Option<u32>,
    ) -> Option<Vec<GoodsStop>> {
        if let Some(stations) = self.sell.get(good) {
            Some(
                stations
                    .iter()
                    .filter_map(|(station, good)| {
                        let stop = GoodsStop { station, good };
                        if let Some(radius) = radius {
                            if stop.distance(x, y) <= radius {
                                Some(stop)
                            } else {
                                None
                            }
                        } else {
                            Some(stop)
                        }
                    })
                    .collect::<Vec<_>>(),
            )
        } else {
            None
        }
    }
}
